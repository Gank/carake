import * as firebase from '../firebase';
const db = firebase.db;
const postCollection = db.collection('posts');

export default class Post {
    async create(make: string, model: string, year: string, price: number) {
        
        return postCollection.add({
            make: make,
            model: model,
            year: year,
            price: price
        }).then(function (postRef) {
            return {
                post_id: postRef.id
            }
        }).catch(function (error) {
            return {
                error: 1,
                error_message: 'There was an error create post'
            }
        });
    }
    async all() {
        return postCollection.get().then(function(querySnapshot){
            let result: any = [];
            querySnapshot.forEach(function(doc) {
                result.push({
                    post: doc.id,
                    data: doc.data()
                });
            });
            return result;
        });
    }

    async get(postID: string){
        let docRef = postCollection.doc(postID);
        let post: any;
        let result: any;
        return docRef.get().then(function(doc) {
            if(doc.exists){
                post = doc.data();
                result = {
                    post: post
                }
            }
            else{
                result = {
                    error: 1,
                    error_message: "No such post!"
                }
            }
            return result;
        })
        .catch(function(error) {
            result = {
                error: 1,
                error_message: "Error! getting document:" + error
            }
            return result;
        });
    }
    async update(postID: string, make: string, model: string, year: string, price: number) {
        let docData = {
            make: make,
            model: model,
            year: year,
            price: price
        }
        return postCollection.doc(postID).set(docData).then(function(){
            return {
                message: "Update post completed"
            };
        });
    }
    async destroy(postID: string) {
        return postCollection.doc(postID).delete().then(function() {
            return {
                message: "Post successfully deleted"
            }
        }).catch(function(error) {
            return {
                error: 1,
                error_message: "Error removing document" + error
            }
        });
    }
}
