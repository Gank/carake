import * as express from 'express';
import * as PostController from '../controllers/PostController';

let router = express.Router();

router.post('/posts', PostController.create);
router.get('/posts', PostController.all);
router.get('/posts/:postID', PostController.get);
router.put('/posts/:postID', PostController.update);
router.delete('/posts/:postID', PostController.destroy);

export { router as ApiV1 };