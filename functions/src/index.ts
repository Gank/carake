import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as firebase from './firebase';
import { ApiV1 } from './routes/api-v1';

const main = express();

main.use('/api/v1', ApiV1);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));

export const webApi = firebase.functions.https.onRequest(main);
