import { Request , Response } from 'express';
import Post from '../models/Post';

const create = async (req: Request, res: Response) => {
    let data = req.body;
    // validate request data
    if (data.make && data.model && data.year && data.price) {
        // add post
        let postRepo = new Post();
        let result: any = await postRepo.create(data.make, data.model, data.year, data.price);
        
        if(result.error){
            res.status(204).json(result);
        }
        else{
            res.status(200).json(result);
        }
    }
    else {
        // error: requir more data
        let response = {
            error: 1,
            error_message: "Please fill in all required data"
        }
        res.status(403);
        res.json(response);
    }
}

const update = async (req: Request, res: Response) => {
    let data = req.body;
    let postID = req.params.postID;
    // validate request data
    if (data.make && data.model && data.year && data.price) {
        // add post
        let postRepo = new Post();
        let result: any = await postRepo.update(postID, data.make, data.model, data.year, data.price);

        if (result.error) {
            res.status(202).json(result);
        }
        else {
            res.status(200).json(result);
        }
    }
    else {
        // error: requir more data
        let response = {
            error: 1,
            error_message: "Please fill in all required data"
        }
        res.status(403);
        res.json(response);
    }
}

const all = async (req: Request, res: Response) => {
    let postRepo = new Post();
    let posts: any = await postRepo.all();

    res.status(200).json(posts);
}

const get = async (req: Request, res: Response) => {
    let postRepo = new Post();
    let postID = req.params.postID;
    if (postID){
        let post: any = await postRepo.get(postID);
        if(post.error){
            res.status(204).json(post);
        }
        else{
            res.status(200).json(post);
        }
    }
    else{
        res.status(403).json("Please fill in id");
    }
}

const destroy = async (req: Request, res: Response) => {
    let postRepo = new Post();
    let postID = req.params.postID;

    let result: any = await postRepo.destroy(postID);
    if(result.error){
        res.status(204).json(result);
    }
    else{
        res.status(200).json(result);
    }
}

export{
    create,
    all,
    get,
    update,
    destroy
}